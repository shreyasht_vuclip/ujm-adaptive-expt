package com.vuclip.aeui.ujmadaptiveexpt;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UjmAdaptiveExptApplicationTests {

	@Test
	public void contextLoads() {
	}

}
