$(document)
		.ready(
				function() {

					$('#seg_policy_dd_ul li').on('click', function() {
						$("#seg_policy_but").text($(this).text());
						$('#hid_val').val($(this).text());
					});

					$('#add_button')
							.click(
									function() {
										var hid_val = $('#hid_val').val();
										if (hid_val === '') {
											alert('Please select a value!');
											return false;
										}

										$("#percent_table > tbody")
												.append(
														"<tr><td class='myClass1'>"
																+ $('#hid_val')
																		.val()
																+ "</td><td><input placeholder ='%age' class ='myClass'/></td><td><button type='button' class='btn btn-danger'>Remove</button></td></tr>");

										$("#seg_policy_but").text('Seg_Policy');
										$('#hid_val').val("");
									});

					$('#data_model_dd_ul li').on('click', function() {
						$("#data_model_but").text($(this).text());
						$('#hid_val2').val($(this).text());
					});

					$(document).on('click', '.btn-danger', function() {
						$(this).closest('tr').remove();
					});

					function performValidations() {
						var numberReg = /^[0-9]+$/;
						var min_perc = $('#min_perc').val();
						var max_perc = $('#max_perc').val();

						if (!numberReg.test(min_perc) || min_perc < 1) {
							alert('Enter proper value for min %');
							return false;

						}

						if (!numberReg.test(max_perc) || max_perc < 1) {
							alert('Enter proper value for min %');
							return false;
						}

						if (Number(max_perc) < Number(min_perc)) {
							alert('max % should be greater than min%');
							return false;
						}

						var add = 0;
						$(".myClass").each(function() {
							add = Number(add) + Number($(this).val());
						});

						if (Number(add) == 0) {
							alert('Please add percentages.');
							return false;

						}

						if (Number(add) > Number(max_perc) || Number(add) > 100) {
							alert('Final %age should be less than max %')
							return false;
						}

						var algo_name = $('#algo_name').val();

						if (algo_name === '') {
							alert('Please input algo_name!');
							return false;
						}

						return true;
					}

					$('#publish_button')
							.click(
									function() {

										if (performValidations()) {
											var algo_name = $('#algo_name')
													.val();
											var dataModel = $('#hid_val2')
													.val();
											var min_perc = $('#min_perc').val();
											var max_perc = $('#max_perc').val();

											var array = [];

											var table = document
													.getElementById('percent_table');

											var rowLength = table.rows.length;

											for (var i = 1; i < rowLength; i += 1) {
												var exptName;
												var percentage;
												var row = table.rows[i];
												var cellLength = row.cells.length;
												for (var y = 0; y < cellLength - 1; y += 1) {
													var cell = $(row.cells[y]);

													if (y === Number(0)) {
														exptName = cell.text();
													}
													if (y === Number(1)) {
														percentage = cell.find(
																'input').val();
													}
												}
												array.push({
													experimentName : exptName,
													percentage : percentage
												});
											}
											var obj = new Object();
											obj.name = algo_name;
											obj.algorithm = dataModel;
											obj.minPercentage = min_perc;
											obj.maxPercentage = max_perc;
											obj.experiments = array;

											var jsonString = JSON
													.stringify(obj);

											$.post("/publish", jsonString,
													function(data) {
														alert(data);
														location.reload();
													});

										}

									});

				});