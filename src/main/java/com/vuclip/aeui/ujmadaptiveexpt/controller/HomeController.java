/**
 * 
 */
package com.vuclip.aeui.ujmadaptiveexpt.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vuclip.aeui.ujmadaptiveexpt.model.UJMData;
import com.vuclip.aeui.ujmadaptiveexpt.pubsub.PubsubOutboundGateway;

/**
 * @author Shreyash
 *
 */
@Controller
public class HomeController {

	@Autowired
	private PubsubOutboundGateway messagingGateway;

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/")
	public String displayHomePage(ModelMap model) throws FileNotFoundException, IOException {
		String exptURL = "https://us-central1-user-journey-management-qa.cloudfunctions.net/function-1";
		ResponseEntity<String> exptURLResponse = restTemplate.getForEntity(exptURL, String.class);
		List<String> exptNames = Arrays
				.asList(exptURLResponse.getBody().replace("[", "").replace("]", "").replaceAll("'", "").split(","));

		model.addAttribute("exptNames", exptNames);

		String dataModelURL = "https://us-central1-user-journey-management-qa.cloudfunctions.net/function-2";
		ResponseEntity<String> dataModelURLResponse = restTemplate.getForEntity(dataModelURL, String.class);
		List<String> dataModelNames = Arrays.asList(
				dataModelURLResponse.getBody().replace("[", "").replace("]", "").replaceAll("'", "").split(","));

		model.addAttribute("dataModelNames", dataModelNames);
		return "/pages/home";
	}

	@PostMapping("/publish")
	public @ResponseBody String publish(@RequestBody String json)
			throws JsonParseException, JsonMappingException, IOException {
		String result = java.net.URLDecoder.decode(json, "UTF-8");
		System.out.println(result.split("=").toString());
		ObjectMapper objectMapper = new ObjectMapper();
		UJMData data = objectMapper.readValue(result, UJMData.class);
		String newJson = objectMapper.writeValueAsString(data);
		//messagingGateway.sendToPubsub(newJson);
		return "Success";
	}
}
