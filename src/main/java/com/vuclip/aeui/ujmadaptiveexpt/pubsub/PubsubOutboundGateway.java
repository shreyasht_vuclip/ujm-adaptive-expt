/**
 * 
 */
package com.vuclip.aeui.ujmadaptiveexpt.pubsub;

import org.springframework.integration.annotation.MessagingGateway;

/**
 * @author shreyashthakare
 *
 */

@MessagingGateway(defaultRequestChannel = "pubsubOutputChannel")
public interface PubsubOutboundGateway {

	void sendToPubsub(String text);
}
