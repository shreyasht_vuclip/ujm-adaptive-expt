package com.vuclip.aeui.ujmadaptiveexpt;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@SpringBootApplication
@ComponentScan(basePackages = { "com.vuclip" })
@EnableAutoConfiguration
public class UjmAdaptiveExptApplication {

	public static void main(String[] args) {
		SpringApplication.run(UjmAdaptiveExptApplication.class, args);
	}
	
}
