package com.vuclip.aeui.ujmadaptiveexpt.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Experiments {

	private String experimentName;

	private String percentage;

}
