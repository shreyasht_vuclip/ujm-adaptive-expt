/**
 * 
 */
package com.vuclip.aeui.ujmadaptiveexpt.model;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Shreyash
 *
 */
@Data
@NoArgsConstructor
public class UJMData {
	
	
//	{
//	  "name": "Sample",
//	  "algorithm": "VideoAdplacementRevenueFocus",
//	  "experiments": [
//	    {
//	      "experimentName": "Infinity_Segment_IndiaIndia_Midroll_A_3Min",
//	      "percentage": 15
//	    },
//	    {
//	      "experimentName": "Infinity_Segment_IndiaIndia_Midroll_A_1Min",
//	      "percentage": 15
//	    },
//		{
//	      "experimentName": "Infinity_Segment_IndiaIndia_Midroll_A_8Min",
//	      "percentage": 15
//	    }
//	  ],
//	  "minPercentage": 5,
//	  "maxPercentage": 50,
//	  "status":"CREATE"
//	}

	private String name;

	private String algorithm;

	private String minPercentage;

	private String maxPercentage;

	private List<Experiments> experiments;

	private String status = "CREATE";

}
